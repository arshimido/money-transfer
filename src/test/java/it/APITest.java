package it;

import com.revolut.moneytransfer.Application;
import com.revolut.moneytransfer.utils.DBUtils;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;


public class APITest {
    
    private static final String TRANSFER_ENDPOINT = "/api/v1/accounts/transfer";
    private static final String ACCOUNT_ENDPOINT = "/api/v1/accounts";

    @Before
    public void setupBefore() {
        System.out.println("Cleaning DB");
        DBUtils.getInstance().stopAll();

        DBUtils.getInstance().startAndConfigure();
        DBUtils.getInstance().configureHibernate();


        new Application().run();
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = 7000;
    }


    @Test
    public void shouldListAllAccounts() {
        when().get(ACCOUNT_ENDPOINT).then()
                .statusCode(200)
                .and()
                .body("", Matchers.hasSize(3));
    }

    @Test
    public void shouldRetrieveAccountSuccessfully() {
        when().get(ACCOUNT_ENDPOINT + "/2").then()
                .statusCode(200)
                .and()
                .body("id", Matchers.equalTo(2))
                .body("ownerId", Matchers.equalTo(22))
                .body("accountNumber", Matchers.equalTo("abc-002-123"))
                .body("balance", Matchers.equalTo(5000.0f))
                .body("currency", Matchers.equalTo("EUR"));
    }


    @Test
    public void shouldResponseWith404WhenAccountDoesNotExist() {
        when().get(ACCOUNT_ENDPOINT + "/2343434").then()
                .statusCode(404)
                .and()
                .body("message", Matchers.equalTo("Entity not found for id: 2343434"));
    }

    @Test
    public void shouldResponseWithUnprocessableEntityForInvalidId() {
        when().get(ACCOUNT_ENDPOINT + "/thisIsInvalidNumber").then()
                .statusCode(400)
                .and()
                .body("message", Matchers.equalTo("BadRequest. Invalid parameter(s)"));
    }

    @Test
    public void shouldResponseWithUnprocessableEntityForTooLongNumber() {
        when().get(ACCOUNT_ENDPOINT + "/9876543210987654321098765432109876543210").then()
                .statusCode(400)
                .and()
                .body("message", Matchers.equalTo("BadRequest. Invalid parameter(s)"));
    }



    @Test
    public void shouldDoTheTransferSuccessfully() {
        String jsonBody = constructTransferRequestJsonBody(1L, 2L, "100", "EUR");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(200);

        // verify new balance
        when().get(ACCOUNT_ENDPOINT + "/1").then()
                .statusCode(200)
                .and().body("balance", Matchers.equalTo(1900.0f));

        // verify new balance
        when().get(ACCOUNT_ENDPOINT + "/2").then()
                .statusCode(200)
                .and().body("balance", Matchers.equalTo(5100.0f));
    }

    @Test
    public void shouldAcceptTransferInDifferentCurrency() {
        String jsonBody = constructTransferRequestJsonBody(1L, 2L, "100", "USD");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(200);

        // verify new balance after currency conversion
        when().get(ACCOUNT_ENDPOINT + "/2").then()
                .statusCode(200)
                .and().body("balance", Matchers.equalTo(5089.58f));

        // verify new balance after currency conversion
        when().get(ACCOUNT_ENDPOINT + "/1").then()
                .statusCode(200)
                .and().body("balance", Matchers.equalTo(1910.42f));
    }


    @Test
    public void shouldNotTransferToSelf() {
        String jsonBody = constructTransferRequestJsonBody(1L, 1L, "100", "EUR");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(422)
                .and().body("message", Matchers.equalTo("Invalid receiver account ID"));

        // verify new balance after currency conversion
        when().get(ACCOUNT_ENDPOINT + "/1").then()
                .statusCode(200)
                .and().body("balance", Matchers.equalTo(2000.0f));
    }


    @Test
    public void shouldNotTransferWhileNotHavingSufficientBalance() {
        String jsonBody = constructTransferRequestJsonBody(3L, 1L, "300", "EUR");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(422)
                .and().body("message", Matchers.equalTo("Insufficient balance"));

        // verify new balance after currency conversion
        when().get(ACCOUNT_ENDPOINT + "/3").then()
                .statusCode(200)
                .and().body("balance", Matchers.equalTo(200.0f));
        // verify new balance after currency conversion
        when().get(ACCOUNT_ENDPOINT + "/1").then()
                .statusCode(200)
                .and().body("balance", Matchers.equalTo(2000.0f));
    }


    @Test
    public void shouldNotTransferUsingUnsupportedCurrency() {
        String jsonBody = constructTransferRequestJsonBody(1L, 2L, "100", "AED");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(422)
                .and().body("message", Matchers.equalTo("Invalid currency"));
    }


    @Test
    public void shouldNotTransferFromNonExistingAccount() {
        String jsonBody = constructTransferRequestJsonBody(1000L, 1L, "100", "EUR");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(422)
                .and().body("message", Matchers.equalTo("Invalid sender account ID"));
    }


    @Test
    public void shouldNotTransferToNonExistingAccount() {
        String jsonBody = constructTransferRequestJsonBody(1L, 200000L, "900", "EUR");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(422)
                .and().body("message", Matchers.equalTo("Invalid receiver account ID"));
    }


    @Test
    public void shouldNotExceedDailyTransferLimit() {
        String jsonBody = constructTransferRequestJsonBody(1L, 2L, "900", "EUR");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(200);

        jsonBody = constructTransferRequestJsonBody(1L, 2L, "120", "EUR");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(422)
                .and().body("message", Matchers.equalTo("Transfer limit exceeded"));
    }

    @Test
    public void shouldDoCurrencyConversionToCalculateDailyLimitAndAcceptTransfer() {
        String jsonBody = constructTransferRequestJsonBody(1L, 2L, "900", "USD");
        System.out.println(jsonBody);
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(200);

        jsonBody = constructTransferRequestJsonBody(1L, 2L, "101", "EUR");
        given().contentType("application/json")
                .body(jsonBody)
                .when().post(TRANSFER_ENDPOINT)
                .then()
                .statusCode(200);
    }

    /**
     * Constructs JSON-String for transferRequest body
     * @param fromId {@code Long} Sender AccountID
     * @param toId {@code Long} Receiver Account ID
     * @param amount {@code String} Amount of transfer as a String
     * @param currency {@code String} Currency of transfer
     *
     * @return {@code String} JSON-String
     */
    private String constructTransferRequestJsonBody(Long fromId, Long toId, String amount, String currency) {
        StringBuilder json = new StringBuilder();
        json.append("{");
        boolean appendComma = false;
        if (fromId != null) {
            json.append("\"fromAccountId\": ").append(fromId);
            appendComma = true;
        }

        if (toId != null) {
            if (appendComma) {
                json.append(", ");
            }

            json.append("\"toAccountId\": ").append(toId);
            appendComma = true;
        }

        if (amount != null) {
            if (appendComma) {
                json.append(", ");
            }

            json.append("\"amount\": ").append(amount);
            appendComma = true;
        }

        if (currency != null) {
            if (appendComma) {
                json.append(", ");
            }

            json.append("\"currency\": ").append( "\"").append(currency).append("\"");
        }

        json.append("}");
        return json.toString();
    }
}
