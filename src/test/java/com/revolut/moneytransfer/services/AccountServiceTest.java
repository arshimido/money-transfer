package com.revolut.moneytransfer.services;

import com.revolut.moneytransfer.daos.AccountDao;
import com.revolut.moneytransfer.dto.TransferRequestDTO;
import com.revolut.moneytransfer.exceptions.*;
import com.revolut.moneytransfer.models.Account;
import com.revolut.moneytransfer.models.Currency;
import com.revolut.moneytransfer.models.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import utils.TestDataUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountDao accountDao;

    @Mock
    private CurrencyExchangeService currencyExchangeService;

    @Mock
    private TransactionService transactionService;

    private AccountService accountService;

    @Before
    public void setup() {
        accountService = new AccountService(accountDao, currencyExchangeService, transactionService);
    }

    @Test
    public void shouldRetrieveListFromDao() {
        List<Account> list = TestDataUtils.createAccountList();
        when(accountDao.list()).thenReturn(list);

        List<Account> result = accountService.list();

        verify(accountDao, times(1)).list();
        Assert.assertEquals(list.get(0).getId(), result.get(0).getId());
    }

    @Test
    public void shouldRetrieveAccountByIdFromDao() {
        Account account = TestDataUtils.createAccount();
        doReturn(Optional.of(account)).when(this.accountDao).findById(1l);

        Optional<Account> result = this.accountService.getAccountById(1l);

        verify(this.accountDao, times(1)).findById(1l);
        Assert.assertTrue(result.isPresent());
        Assert.assertEquals(account, result.get());
    }

    @Test
    public void shouldNotFindAccount() {
        doReturn(Optional.empty()).when(this.accountDao).findById(1l);
        Optional<Account> result = this.accountService.getAccountById(1l);

        verify(this.accountDao, times(1)).findById(1l);
        Assert.assertFalse(result.isPresent());
    }

    @Test
    public void shouldExecuteTransferSuccessfully() throws InSufficientBalanceException, CurrencyNotSupportedException,
            AccountDoesNotExistException, TransferLimitExceeded, EntityNotFoundException {

        Long fromId = 1l, toId = 2l;
        TransferRequestDTO transferRequestDTO = TestDataUtils.createTransferRequestDTO(fromId, toId, BigDecimal.valueOf(100), "EUR");
        Account fromAccount = TestDataUtils.createAccount(fromId, BigDecimal.valueOf(1000));
        Account toAccount = TestDataUtils.createAccount(toId, BigDecimal.valueOf(1000));
        Transaction trx = TestDataUtils.createTransaction(1l, fromAccount, toAccount, transferRequestDTO.getAmount(),
                Currency.valueOf(transferRequestDTO.getCurrency()));

        doReturn(Optional.of(fromAccount)).when(this.accountDao).findById(fromId);
        doReturn(Optional.of(toAccount)).when(this.accountDao).findById(toId);
        doReturn(new ArrayList<>()).when(this.transactionService).findTransferTransactionsForAccountAsSender(eq(fromAccount), any());
        doNothing().when(this.accountDao).executeTransfer(fromId, toId, trx.getId());
        doReturn(trx).when(this.transactionService).createNewTransferTransaction(fromAccount, toAccount,
                transferRequestDTO.getAmount(), Currency.valueOf(transferRequestDTO.getCurrency()));

        this.accountService.transferMoney(transferRequestDTO);

        verify(this.accountDao, times(2)).findById(fromId);
        verify(this.accountDao, times(2)).findById(toId);
        verify(this.accountDao, times(1)).executeTransfer(fromId, toId, trx.getId());
        verify(this.transactionService, times(1)).findTransferTransactionsForAccountAsSender(eq(fromAccount), any());
    }

    @Test(expected = InSufficientBalanceException.class)
    public void shouldThrowExceptionForInsufficientBalance() throws InSufficientBalanceException,
            CurrencyNotSupportedException, AccountDoesNotExistException, TransferLimitExceeded, EntityNotFoundException {

        Long fromId = 1l, toId = 2l;
        TransferRequestDTO transferRequestDTO = TestDataUtils.createTransferRequestDTO(fromId, toId, BigDecimal.valueOf(500), "EUR");
        Account fromAccount = TestDataUtils.createAccount(fromId, BigDecimal.valueOf(100));
        Account toAccount = TestDataUtils.createAccount(toId, BigDecimal.valueOf(1000));

        doReturn(Optional.of(fromAccount)).when(this.accountDao).findById(fromId);
        doReturn(Optional.of(toAccount)).when(this.accountDao).findById(toId);

        this.accountService.transferMoney(transferRequestDTO);
    }

    @Test(expected = CurrencyNotSupportedException.class)
    public void shouldThrowExceptionForUnsupportedCurrency() throws InSufficientBalanceException,
            CurrencyNotSupportedException, AccountDoesNotExistException, TransferLimitExceeded, EntityNotFoundException {

        Long fromId = 1l, toId = 2l;
        TransferRequestDTO transferRequestDTO = TestDataUtils.createTransferRequestDTO(fromId, toId, BigDecimal.valueOf(500), "RUB");
        Account fromAccount = TestDataUtils.createAccount(fromId, BigDecimal.valueOf(1000));
        Account toAccount = TestDataUtils.createAccount(toId, BigDecimal.valueOf(1000));

        doReturn(Optional.of(fromAccount)).when(this.accountDao).findById(fromId);
        doReturn(Optional.of(toAccount)).when(this.accountDao).findById(toId);

        this.accountService.transferMoney(transferRequestDTO);
    }

    @Test(expected = AccountDoesNotExistException.class)
    public void shouldThrowExceptionForTransferringToSelf() throws InSufficientBalanceException,
            CurrencyNotSupportedException, AccountDoesNotExistException, TransferLimitExceeded, EntityNotFoundException {

        Long fromId = 1l, toId = 1l;
        TransferRequestDTO transferRequestDTO = TestDataUtils.createTransferRequestDTO(fromId, toId, BigDecimal.valueOf(500), "RUB");
        Account fromAccount = TestDataUtils.createAccount(fromId, BigDecimal.valueOf(1000));

        doReturn(Optional.of(fromAccount)).when(this.accountDao).findById(fromId);


        this.accountService.transferMoney(transferRequestDTO);
    }

    @Test(expected = AccountDoesNotExistException.class)
    public void shouldThrowExceptionForTransferringFromNonExistingAccount() throws InSufficientBalanceException,
            CurrencyNotSupportedException, AccountDoesNotExistException, TransferLimitExceeded, EntityNotFoundException {

        Long fromId = 1l, toId = 2l;
        TransferRequestDTO transferRequestDTO = TestDataUtils.createTransferRequestDTO(fromId, toId, BigDecimal.valueOf(500), "RUB");

        this.accountService.transferMoney(transferRequestDTO);
    }


    @Test(expected = AccountDoesNotExistException.class)
    public void shouldThrowExceptionForTransferringToNonExistingAccount() throws InSufficientBalanceException,
            CurrencyNotSupportedException, AccountDoesNotExistException, TransferLimitExceeded, EntityNotFoundException {

        Long fromId = 1l, toId = 2l;
        TransferRequestDTO transferRequestDTO = TestDataUtils.createTransferRequestDTO(fromId, toId, BigDecimal.valueOf(500), "RUB");
        Account fromAccount = TestDataUtils.createAccount(fromId, BigDecimal.valueOf(1000));

        doReturn(Optional.of(fromAccount)).when(this.accountDao).findById(fromId);

        this.accountService.transferMoney(transferRequestDTO);
    }

}
