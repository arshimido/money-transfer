package com.revolut.moneytransfer.services;

import com.revolut.moneytransfer.daos.AccountDao;
import com.revolut.moneytransfer.daos.TransactionDao;
import com.revolut.moneytransfer.dto.TransferRequestDTO;
import com.revolut.moneytransfer.exceptions.*;
import com.revolut.moneytransfer.models.Account;
import com.revolut.moneytransfer.models.Currency;
import com.revolut.moneytransfer.models.Transaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.Invocation;
import org.mockito.junit.MockitoJUnitRunner;
import utils.TestDataUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @Mock
    private TransactionDao transactionDao;

    @Mock
    private CurrencyExchangeService currencyExchangeService;

    private TransactionService transactionService;


    @Before
    public void setup() {
        transactionService = new TransactionService(transactionDao, currencyExchangeService);
    }

    @Test
    public void shouldRetrieveListOfTransactionsFromDao() {
        Long fromId = 1l;
        Account fromAccount = TestDataUtils.createAccount(fromId, BigDecimal.valueOf(1000));

        LocalDateTime now = LocalDateTime.now();


        doReturn(new ArrayList<>()).when(this.transactionDao).findTransferTransactionsFromAccountSince(fromAccount, now);

        List<Transaction> list = this.transactionService.findTransferTransactionsForAccountAsSender(fromAccount, now);

        Assert.assertTrue(list.isEmpty());
        verify(this.transactionDao, times(1)).findTransferTransactionsFromAccountSince(fromAccount, now);
    }


    @Test
    public void shouldCreateAndSaveTransaction() {
        Long fromId = 1l, toId = 2l;
        BigDecimal transferAmount = BigDecimal.valueOf(100);
        Account fromAccount = TestDataUtils.createAccount(fromId, BigDecimal.valueOf(1000));
        Account toAccount = TestDataUtils.createAccount(toId, BigDecimal.valueOf(1000));

        doReturn(1L).when(this.transactionDao).saveTransaction(any());
        doReturn(Optional.of(new Transaction())).when(this.transactionDao).findById(any());


        this.transactionService.createNewTransferTransaction(fromAccount, toAccount, transferAmount, Currency.EUR);


        Collection invocations = Mockito.mockingDetails(this.transactionDao).getInvocations();
        // first invocation is "saveTransaction" method
        Invocation inv = (Invocation) invocations.iterator().next();

        Transaction trx = (Transaction) inv.getRawArguments()[0];
        Assert.assertEquals(invocations.size(), 2);
        Assert.assertEquals(trx.getSender(), fromAccount);
        Assert.assertEquals(trx.getAmount(), transferAmount);
        Assert.assertEquals(trx.getReceiver(), toAccount);
        Assert.assertEquals(trx.getAmountInSenderCurrency(), transferAmount);
        Assert.assertEquals(trx.getAmountInReceiverCurrency(), transferAmount);
        Assert.assertEquals(trx.getSenderCurrency(), fromAccount.getCurrency());
        Assert.assertEquals(trx.getReceiverCurrency(), toAccount.getCurrency());
    }



    @Test
    public void shouldDoCurrencyConversionAndCreateAndSaveTransaction() {
        Long fromId = 1l, toId = 2l;
        BigDecimal transferAmount = BigDecimal.valueOf(100);
        Account fromAccount = TestDataUtils.createAccount(fromId, BigDecimal.valueOf(1000));
        Account toAccount = TestDataUtils.createAccount(toId, BigDecimal.valueOf(1000), Currency.USD);
        BigDecimal exchangeRate = BigDecimal.valueOf(1.2);

        doReturn(exchangeRate).when(this.currencyExchangeService).getExchangeRate(any(), any());
        doReturn(1L).when(this.transactionDao).saveTransaction(any());
        doReturn(Optional.of(new Transaction())).when(this.transactionDao).findById(any());


        this.transactionService.createNewTransferTransaction(fromAccount, toAccount, transferAmount, Currency.EUR);


        Collection invocations = Mockito.mockingDetails(this.transactionDao).getInvocations();
        // first invocation is "saveTransaction" method
        Invocation inv = (Invocation) invocations.iterator().next();

        Transaction trx = (Transaction) inv.getRawArguments()[0];
        Assert.assertEquals(invocations.size(), 2);
        Assert.assertEquals(trx.getSender(), fromAccount);
        Assert.assertEquals(trx.getAmount(), transferAmount);
        Assert.assertEquals(trx.getReceiver(), toAccount);
        Assert.assertEquals(trx.getAmountInSenderCurrency(), transferAmount);
        Assert.assertEquals(trx.getAmountInReceiverCurrency(), transferAmount.multiply(exchangeRate));
        Assert.assertEquals(trx.getSenderCurrency(), fromAccount.getCurrency());
        Assert.assertEquals(trx.getReceiverCurrency(), toAccount.getCurrency());
    }



}
