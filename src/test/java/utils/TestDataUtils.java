package utils;

import com.revolut.moneytransfer.dto.TransferRequestDTO;
import com.revolut.moneytransfer.models.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TestDataUtils {

    public static Account createAccount() {
        return createAccount(1l, BigDecimal.valueOf(1000));
    }

    public static Account createAccount(Long id, BigDecimal balance) {
        return createAccount(id, balance, Currency.EUR);
    }

    public static Account createAccount(Long id, BigDecimal balance, Currency currency) {
        return createAccount(id, 1l, balance, "account#123", LocalDateTime.now().minusYears(2), currency);
    }

    public static Account createAccount(Long id, Long ownerId, BigDecimal balance, String accountNumber, LocalDateTime creationDate, Currency currency) {
        Account account = new Account();
        account.setId(id);
        account.setOwnerId(ownerId);
        account.setBalance(balance);

        account.setAccountNumber(accountNumber);
        account.setCreationDate(creationDate);
        account.setCurrency(currency);
        return account;
    }

    public static List<Account> createAccountList() {
        List<Account> list = new ArrayList<>();
        list.add(createAccount());
        return list;
    }



    public static TransferRequestDTO createTransferRequestDTO(Long fromId, Long toId, BigDecimal amount, String currency) {
        TransferRequestDTO transferRequestDTO = new TransferRequestDTO();
        transferRequestDTO.setAmount(amount);
        transferRequestDTO.setCurrency(currency);
        transferRequestDTO.setFromAccountId(fromId);
        transferRequestDTO.setToAccountId(toId);
        return transferRequestDTO;
    }


    public static Transaction createTransaction(Long id, Account sender, Account receiver, BigDecimal amount, Currency currency) {
        return createTransaction(id, TransactionType.TRANSFER, TransactionStatus.PENDING, sender, receiver, amount, currency,
                amount, amount, BigDecimal.ONE, BigDecimal.ONE, LocalDateTime.now());
    }

    public static Transaction createTransaction(Long id, TransactionType type, TransactionStatus status, Account sender,
            Account receiver, BigDecimal amount, Currency currency, BigDecimal amountInSenderCurrency,
            BigDecimal amountInReceiverCurrency, BigDecimal exchangeRateToSenderCurrency,
                                                BigDecimal exchangeRateToReceiverCurrency, LocalDateTime creationDate) {

        Transaction trx = new Transaction();
        trx.setId(id);
        trx.setType(TransactionType.TRANSFER);
        trx.setStatus(TransactionStatus.PENDING);
        trx.setSender(sender);
        trx.setReceiver(receiver);
        trx.setAmount(amount);
        trx.setCurrency(currency);
        trx.setAmountInSenderCurrency(amountInSenderCurrency);
        trx.setAmountInReceiverCurrency(amountInReceiverCurrency);
        trx.setExchangeRateToSenderCurrency(exchangeRateToSenderCurrency);
        trx.setExchangeRateToReceiverCurrency(exchangeRateToReceiverCurrency);
        trx.setCreationDate(creationDate);
        return trx;
    }
}
