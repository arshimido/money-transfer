package com.revolut.moneytransfer.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.revolut.moneytransfer.models.Account;
import com.revolut.moneytransfer.models.Currency;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {
    private Long id;
    private Long ownerId;
    private String accountNumber;
    private BigDecimal balance;
    private Currency currency;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationDate;

    public static AccountDTO fromAccount(Account account) {
        return new AccountDTO(account.getId(), account.getOwnerId(), account.getAccountNumber(),
                account.getBalance(), account.getCurrency(), account.getCreationDate());
    }
}
