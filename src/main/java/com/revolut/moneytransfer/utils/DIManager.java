package com.revolut.moneytransfer.utils;

import com.revolut.moneytransfer.controllers.AccountController;
import com.revolut.moneytransfer.daos.AccountDao;
import com.revolut.moneytransfer.daos.TransactionDao;
import com.revolut.moneytransfer.services.AccountService;
import com.revolut.moneytransfer.services.CurrencyExchangeService;
import com.revolut.moneytransfer.services.InMemoryCurrencyExchangeService;
import com.revolut.moneytransfer.services.TransactionService;

import java.util.HashMap;
import java.util.Map;

/**
 * Dependency-Injection Manager
 *
 * Responsible for required instances initialization (DAOs, Services, Controllers .. etc) and DI.
 */
public class DIManager {

    private static DIManager dependencyInjectionManager;

    private TransactionDao transactionDao;
    private AccountDao accountDao;
    private AccountService accountService;
    private TransactionService transactionService;
    private CurrencyExchangeService currencyExchangeService;
    private AccountController accountController;

    private Map<Class, Object> objectStore;

    private DIManager() {
    }

    public static synchronized DIManager getInstance() {
        if (dependencyInjectionManager == null) {
            dependencyInjectionManager = new DIManager();
        }
        return dependencyInjectionManager;
    }

    public void init() {
        transactionDao = new TransactionDao();
        accountDao = new AccountDao(transactionDao);
        currencyExchangeService = new InMemoryCurrencyExchangeService();
        transactionService = new TransactionService(transactionDao, currencyExchangeService);
        accountService = new AccountService(accountDao, currencyExchangeService, transactionService);
        accountController = new AccountController(accountService);

        objectStore = new HashMap<>();
        objectStore.put(AccountDao.class, accountDao);
        objectStore.put(AccountService.class, accountService);
        objectStore.put(TransactionDao.class, transactionDao);
        objectStore.put(TransactionService.class, transactionService);
        objectStore.put(CurrencyExchangeService.class, currencyExchangeService);
        objectStore.put(AccountController.class, accountController);
    }

    /**
     * Retrieves instance of specified class {@code clazz}.
     *
     * @throws {@code IllegalStateException} if no instances found of specified class {@code clazz}.
     *
     * @param clazz {@code Class} type of required instance.
     * @param <Any>
     *
     * @return instance of specified class type {@code clazz}
     */
    public <Any> Any getInstanceOf(Class clazz) {
        Any obj = (Any) objectStore.get(clazz);
        if (obj == null) throw new IllegalStateException("No initialized instance of class: " + clazz);
        return obj;
    }


}
