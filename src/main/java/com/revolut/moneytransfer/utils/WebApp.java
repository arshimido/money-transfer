package com.revolut.moneytransfer.utils;

import com.revolut.moneytransfer.controllers.AccountController;
import com.revolut.moneytransfer.dto.ErrorDTO;
import io.javalin.Javalin;
import io.javalin.http.InternalServerErrorResponse;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

import static io.javalin.apibuilder.ApiBuilder.*;

public class WebApp {


    private static WebApp webApp;
    private WebApp() {}

    private Javalin app;

    public static synchronized WebApp getInstance() {
        if (webApp == null) {
            webApp = new WebApp();
        }
        return webApp;
    }

    /**
     * Configuring Javalin app, and starting web server.
     */
    public synchronized void start() {
        if (app == null) {
            // Initialize Instances
            DIManager.getInstance().init();

            app = Javalin.create(config -> {
                config.registerPlugin(getConfiguredOpenApiPlugin());
                config.defaultContentType = "application/json";
            }).exception(Exception.class, (e, context) -> {
                e.printStackTrace();
                context.json(new ErrorDTO(e.getMessage())).status(422);
            }).error(400, context -> context.json(new ErrorDTO("BadRequest. Invalid parameter(s)")));


            app.get("/", ctx -> ctx.result("Hello World"));
            this.configureRoutes(app);
            app.start(ApplicationProperties.PORT);
        }
    }

    /**
     * Configuring application routes
     *
     * @param app {@code Javalin} application instance to configure.
     */
    private void configureRoutes(Javalin app) {
        DIManager diManager = DIManager.getInstance();

        AccountController accountController = diManager.getInstanceOf(AccountController.class);

        app.routes(() -> {
            path(ApplicationProperties.BASE_PATH, () -> {
                path("accounts", () -> {
                    get(accountController::list);
                    path("transfer", () -> {
                        post(accountController::transferMoney);
                    });
                    path(":accountId", () -> {
                        get(accountController::getAccount);
                    });
                });

            });
        });
    }

    /**
     * Configure documentation endpoint.
     *
     * @return {@code OpenAPIPlugin} a configured instance.
     */
    private OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").description("Moneytransfer app API");
        OpenApiOptions options = new OpenApiOptions(info)
                .activateAnnotationScanningFor("com.revolut.moneytransfer")
                .path("/openapi") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
                .defaultDocumentation(doc -> {
                    doc.json("500", InternalServerErrorResponse.class);
                    doc.json("503", InternalServerErrorResponse.class);
                });
        return new OpenApiPlugin(options);
    }

}
