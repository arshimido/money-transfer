package com.revolut.moneytransfer.utils;


/**
 * Global application properties including business properties.
 *
 *
 * Ideally those properties should be read from Environment variables.
 * TODO: change it to ENV VARIABLE
 */
public class ApplicationProperties {

    public static final String BASE_PATH = "/api/v1";
    public static final int PORT = 7000;
    public static final int DAILY_TRANSFER_LIMIT = 1000;
    public static final int MONTHLY_TRANSFER_LIMIT = 10000;
}
