package com.revolut.moneytransfer.utils;

import org.h2.tools.Server;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * DB Utility class. To start/stop DB server and DB-WebServer.
 */
public class DBUtils {

    private static final Logger logger = LoggerFactory.getLogger(DBUtils.class);

    private static DBUtils dbUtils;

    private SessionFactory sessionFactory;
    private Server h2Server;
    private Server h2WebServer;

    private DBUtils() {
        // disable initialization
    }

    public static DBUtils getInstance() {
        if (dbUtils == null) {
            dbUtils = new DBUtils();
        }
        return dbUtils;
    }

    public void startAndConfigure() {
        this.startDB();
        this.configureHibernate();
    }

    public void startDB() {
        if (h2Server != null) {
            logger.info("H2 DB Server already running on port: " + this.h2Server.getPort());
            return;
        }
        try {
            h2Server = Server.createTcpServer("-tcpAllowOthers", "-ifNotExists").start();
            h2WebServer = Server.createWebServer().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void configureHibernate() {
        if (sessionFactory != null) {
            logger.info("Hibernate SessionFactory already initialized");
            return;
        }
        try {
            sessionFactory = new Configuration()
                    .configure(new File("src/main/resources/hibernate-config.xml"))
                    .buildSessionFactory();

        } catch (Exception ex) {
            logger.error("Failed to initialize hibernate session factory");
            throw new ExceptionInInitializerError(ex);
        }
    }

    public void stopAll() {
        if (this.sessionFactory != null) {
            this.sessionFactory.close();
            this.sessionFactory = null;
        }

        if (this.h2Server != null) {
            this.h2Server.stop();
            this.h2Server.shutdown();
            this.h2Server = null;
        }

        if (h2WebServer != null) {
            this.h2WebServer.stop();
            this.h2WebServer.shutdown();
            this.h2WebServer = null;
        }
    }

    public SessionFactory getSessionFactory() {
        if (this.sessionFactory == null) {
            throw new IllegalStateException("Session factory not initialized");
        }
        return this.sessionFactory;
    }
}
