package com.revolut.moneytransfer.exceptions;

public class CurrencyNotSupportedException extends Exception {
    public CurrencyNotSupportedException(String message) {
        super(message);
    }
}
