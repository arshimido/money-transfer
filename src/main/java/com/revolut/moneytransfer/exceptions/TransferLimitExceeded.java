package com.revolut.moneytransfer.exceptions;

public class TransferLimitExceeded extends Exception {

    public TransferLimitExceeded(String message) {
        super(message);
    }
}
