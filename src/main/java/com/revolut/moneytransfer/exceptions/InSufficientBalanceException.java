package com.revolut.moneytransfer.exceptions;

public class InSufficientBalanceException extends Exception {
    
    public InSufficientBalanceException(String message) {
        super(message);
    }
}
