package com.revolut.moneytransfer;

import com.revolut.moneytransfer.utils.DBUtils;
import com.revolut.moneytransfer.utils.WebApp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    private static final Logger logger = LoggerFactory.getLogger(Application.class);


    /**
     * Starting DB and initializing web app.
     */
    public void run() {
        logger.info("Starting WebApp");
        WebApp.getInstance().start();
    }


    public static void main(String[] args) {
        DBUtils dbUtils = DBUtils.getInstance();
        dbUtils.startDB();
        dbUtils.configureHibernate();

        Application application = new Application();
        application.run();
    }

}
