package com.revolut.moneytransfer.daos;

import com.revolut.moneytransfer.exceptions.InSufficientBalanceException;
import com.revolut.moneytransfer.models.Account;
import com.revolut.moneytransfer.models.Transaction;
import com.revolut.moneytransfer.utils.DBUtils;
import org.hibernate.FlushMode;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * Data Access Object to handle DB operations for {@code Account} entity.
 */
public class AccountDao {


    private static final Logger logger = LoggerFactory.getLogger(AccountDao.class);

    private TransactionDao transactionDao;

    public AccountDao(TransactionDao transactionDao) {
        this.transactionDao = transactionDao;
    }

    /**
     * List all accounts
     *
     * @return {@code List<Account>} list of all accounts.
     */
    public List<Account> list() {
        SessionFactory sessionFactory = DBUtils.getInstance().getSessionFactory();

        try (Session session = sessionFactory.openSession()) {
            TypedQuery<Account> query = session.getNamedQuery("account.list-all");
            List<Account> accounts = query.getResultList();

            session.close();
            return accounts;
        }
    }

    /**
     * Retrieves {@code Account} by id
     *
     * @param id {@code Long} id of account to retrieve
     *
     * @return {@code Optional<Account>} contains retrieved {@code Account}. Or empty optional if none exists.
     */
    public Optional<Account> findById(Long id) {
        SessionFactory sessionFactory = DBUtils.getInstance().getSessionFactory();

        try (Session session = sessionFactory.openSession()) {
            TypedQuery<Account> query = session.getNamedQuery("account.find-by-id");
            query.setParameter("id", id);
            Account account = query.getSingleResult();
            return Optional.ofNullable(account);
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }


    /**
     * Executes transfer request. Reduces the balance of sender account, and increases the balance of receiver account.
     * And also updates corresponding {@code Transaction}
     *
     * @param fromAccountId {@code Long} Sender account ID
     * @param toAccountId {@code Long} Receiver account ID
     * @param transactionId  {@code Long} Corresponding Transaction ID
     *
     * @throws InSufficientBalanceException if sender account doesn't have enough balance.
     */
    public void executeTransfer(Long fromAccountId, Long toAccountId, Long transactionId) throws InSufficientBalanceException {
        logger.info("executing transaction {}", transactionId);
        SessionFactory sessionFactory = DBUtils.getInstance().getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            session.setHibernateFlushMode(FlushMode.MANUAL);
            session.beginTransaction();

            Account from = session.get(Account.class, fromAccountId, LockMode.PESSIMISTIC_WRITE);
            Account to = session.get(Account.class, toAccountId, LockMode.PESSIMISTIC_WRITE);
            Optional<Transaction> transferTransactionOptional = this.transactionDao.findByIdForUpdate(transactionId, session);

            if (from == null || to == null || !transferTransactionOptional.isPresent()) {
                logger.error("Could not retrieve required entities to execute transfer.");
                throw new IllegalStateException("Could not retrieve required entities to execute transfer.");
            }

            logger.info("Accounts and Transaction retrieved");
            Transaction transferTransaction = transferTransactionOptional.get();

            // ensure getting fresh data NOT from cache
            session.refresh(from);
            session.refresh(to);
            session.refresh(transferTransaction);

            // verify balance
            logger.info("Verifying balance");
            this.doPrePersistVerifications(from, transferTransaction);
            logger.info("Balance is sufficient");

            logger.info("Updating balances");
            from.setBalance(from.getBalance().subtract(transferTransaction.getAmountInSenderCurrency()));
            to.setBalance(to.getBalance().add(transferTransaction.getAmountInReceiverCurrency()));

            // update balance
            session.save(from);
            logger.info("Sender account(Id: {}) balance updated", from.getId());

            session.save(to);
            logger.info("Receiver account(Id: {}) balance updated", to.getId());
            this.transactionDao.markTransactionSucceededAndSave(transferTransaction, session);
            session.flush();
            session.getTransaction().commit();
            logger.info("Transaction committed. Transfer executed successfully");
        }
    }

    /**
     * Verify sender's account balance and currency.
     *
     * @param account {@code Account} Sender account
     * @param transaction {@code Transaction} corresponding transaction
     *
     * @throws InSufficientBalanceException if sender's balance is insufficient.
     */
    private void doPrePersistVerifications(Account account, Transaction transaction) throws InSufficientBalanceException {
        if (account.getBalance().compareTo(transaction.getAmountInSenderCurrency()) < 0) {
            throw new InSufficientBalanceException("Insufficient balance");
        }

        // check if account currency had changed concurrently (Not actually sure if this is 'valid' Business case).
        if (!account.getCurrency().equals(transaction.getSenderCurrency())) {
            throw new IllegalStateException("Account currency has been changed");
        }
    }
}
