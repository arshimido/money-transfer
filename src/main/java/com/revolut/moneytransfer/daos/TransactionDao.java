package com.revolut.moneytransfer.daos;

import com.revolut.moneytransfer.models.Account;
import com.revolut.moneytransfer.models.Transaction;
import com.revolut.moneytransfer.models.TransactionStatus;
import com.revolut.moneytransfer.models.TransactionType;
import com.revolut.moneytransfer.utils.DBUtils;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


/**
 * Data Access Object to handle DB operations for {@code Transaction} entity.
 */
public class TransactionDao {

    private static final Logger logger = LoggerFactory.getLogger(TransactionDao.class);

    public TransactionDao() {}

    /**
     * Retrieves List of 'Transfer' transactions were done by Account {@code sender}
     * since specified datetime {@code since}
     *
     * @param account {@code Account} account to search for transactions as a sender
     * @param since {@code LocalDateTime} beginning of time window to search within.
     *
     * @return {@code List<Transactions>}
     */
    public List<Transaction> findTransferTransactionsFromAccountSince(Account account, LocalDateTime since) {
        SessionFactory sessionFactory = DBUtils.getInstance().getSessionFactory();
        try (Session session = sessionFactory.openSession()) {
            TypedQuery<Transaction> query = session.getNamedQuery("transaction.transfers-from-account-after-date");
            query.setParameter("senderId", account.getId());
            query.setParameter("trxType", TransactionType.TRANSFER.ordinal());
            query.setParameter("excludedStatus", TransactionStatus.FAILED.ordinal());
            query.setParameter("since", since);
            List<Transaction> transactionList = query.getResultList();
            return transactionList;
        }
    }

    /**
     * Saves specified {@code Transaction} instance to DB
     * @param transaction {@code Transaction} instance to save.
     * @return {@code Long} generated ID of saved entity.
     */
    public Long saveTransaction(Transaction transaction) {
        SessionFactory sessionFactory = DBUtils.getInstance().getSessionFactory();
        try (Session session = sessionFactory.openSession()) {

            org.hibernate.Transaction dbTransaction = session.beginTransaction();
            Long id = (Long) session.save(transaction);
            dbTransaction.commit();
            return id;
        }
    }


    /**
     * Updates specified transaction instance
     * @param transaction {@code Transaction} instance to update.
     */
    public void updateTransaction(Transaction transaction) {
        SessionFactory sessionFactory = DBUtils.getInstance().getSessionFactory();
        try (Session session = sessionFactory.openSession()) {

            org.hibernate.Transaction dbTransaction = session.beginTransaction();
            session.update(transaction);
            dbTransaction.commit();
        }
    }

    /**
     * Retrieves {@code Transaction} by id
     * @param id {@code Long} id of entity
     * @return {@code Optional<Transaction>} for found entity. or empty Optional if non found.
     */
    public Optional<Transaction> findById(Long id) {
        logger.info("retrieving transaction for id: {}", id);
        SessionFactory sessionFactory = DBUtils.getInstance().getSessionFactory();

        try (Session session = sessionFactory.openSession()) {
            TypedQuery<Transaction> query = session.getNamedQuery("transaction.find-by-id");
            query.setParameter("id", id);
            Transaction transaction = query.getSingleResult();
            return Optional.ofNullable(transaction);
        } catch (NoResultException e) {
            logger.info("No transaction found for id: {}", id);
            return Optional.empty();
        }
    }

    /**
     * Retrives tranasaction by ID, and applies a {@code LockMode.PESSIMISTIC_WRITE} lock to that row.
     *
     * @param id {@code Long} id of Transaction
     * @param session {@code Session} current hibernate session
     *
     * @return @return {@code Optional<Transaction>} for found entity. or empty Optional if non found.
     */
    public Optional<Transaction> findByIdForUpdate(Long id, Session session) {
        Transaction transaction = session.get(Transaction.class, id, LockMode.PESSIMISTIC_WRITE);
        return Optional.ofNullable(transaction);

    }

    /**
     * Marks transaction as succeeded by updating transaction status to {@code SUCCEEDED}.
     *
     * @param transaction {@code Transaction} instance to update.
     * @param session  {@code Session} current hibernate session
     */
    public void markTransactionSucceededAndSave(Transaction transaction, Session session) {
        transaction.setStatus(TransactionStatus.SUCCEEDED);
        session.save(transaction);
    }
}
