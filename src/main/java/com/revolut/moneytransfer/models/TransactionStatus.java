package com.revolut.moneytransfer.models;

public enum TransactionStatus {
    PENDING,        // 0
    SUCCEEDED,      // 1
    FAILED          // 2
}
