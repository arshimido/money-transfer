package com.revolut.moneytransfer.models;

/**
 * Currencies supported by our system.
 *
 * Ideally this should be Database entity. So we can easily integrate more Currencies into our System.
 */
public enum Currency {
    EUR,
    USD,
    GBP
}
