package com.revolut.moneytransfer.models;

public enum TransactionType {
    /**
     * Card payment or online payment
     */
    PAYMENT,


    /**
     * Money Trasnfer
     */
    TRANSFER,


    /**
     * Money withdrawal fees. i.e. ATM fees .. etc
     */
    WAITHDRAWAL,


    /**
     * Bank fees. i.e. transaction processing fees or International withdrawals fees.
     */
    FEES
}
