package com.revolut.moneytransfer.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "account")
@NamedQueries(
        {
            @NamedQuery(name = "account.list-all", query = "from  Account"),
            @NamedQuery(name = "account.find-by-id", query = "from Account where id = :id")
        }
)
public class Account {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "owner_id")
    private Long ownerId;

    @Column(name = "account_number")
    private String accountNumber;

    @Column(precision = 19, scale = 4)
    private BigDecimal balance;

    @Enumerated(EnumType.ORDINAL)
    private Currency currency;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    public String toString() {
        return "Account: [ID: " + this.getId() +
                ", OwnerId: " + this.getOwnerId() + ", AccountNumber: " + this.getAccountNumber() +
                ", Balance: " + this.getBalance() + ", Currency: " + this.currency +
                ", CreationDate: " + this.getCreationDate() + "]";
    }
}
