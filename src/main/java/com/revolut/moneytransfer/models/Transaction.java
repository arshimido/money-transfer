package com.revolut.moneytransfer.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "transaction")
@NamedQueries({
        @NamedQuery(name = "transaction.transfers-from-account-after-date",
                query = "from Transaction as trx where trx.sender.id = :senderId and trx.creationDate > :since and trx.type = :trxType and trx.status <> :excludedStatus"),
        @NamedQuery(name = "transaction.find-by-id", query = "from Transaction where id = :id"),
        @NamedQuery(name = "transaction.list-all", query = "from Transaction")
})
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "smallint")
    private TransactionType type;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "smallint")
    private TransactionStatus status;

    @ManyToOne
    private Account sender;

    @ManyToOne
    private Account receiver;

    @Column(precision = 19, scale = 4)
    private BigDecimal amount;

    @Enumerated(value = EnumType.ORDINAL)
    private Currency currency;

    private BigDecimal amountInSenderCurrency;

    private BigDecimal amountInReceiverCurrency;

    @Enumerated(value = EnumType.ORDINAL)
    private Currency senderCurrency;

    @Enumerated(value = EnumType.ORDINAL)
    private Currency receiverCurrency;

    private BigDecimal exchangeRateToSenderCurrency;

    private BigDecimal exchangeRateToReceiverCurrency;

    private LocalDateTime creationDate;

    @PrePersist
    protected void onCreate() {
        creationDate = LocalDateTime.now();
    }
}
