package com.revolut.moneytransfer.services;

import com.revolut.moneytransfer.exceptions.CurrencyNotSupportedException;
import com.revolut.moneytransfer.models.Currency;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * Currency exchange rate
 */
public class InMemoryCurrencyExchangeService implements CurrencyExchangeService {

    private final Map<String, BigDecimal> exchangeRateMap;

    public InMemoryCurrencyExchangeService() {
        this.exchangeRateMap = new HashMap<>();

        this.exchangeRateMap.put("EUR-USD", new BigDecimal("1.1162"));
        this.exchangeRateMap.put("EUR-GBP", new BigDecimal("0.8537"));
        this.exchangeRateMap.put("USD-EUR", new BigDecimal("0.8958"));
        this.exchangeRateMap.put("USD-GBP", new BigDecimal("0.7649"));
        this.exchangeRateMap.put("GBP-EUR", new BigDecimal("1.1712"));
        this.exchangeRateMap.put("GBP-USD", new BigDecimal("1.3073"));
    }

    /**
     * Retrieves exchange rate from Currency to another
     *
     * @param fromCurrency {@code Currency} currency to retrieve exchange rate from
     * @param toCurrency {@code Currency}
     *
     * @return {@code BigDecimal} exchange rate from {@code fromCurrency} to {@code toCurrency}
     */
    public BigDecimal getExchangeRate(Currency fromCurrency, Currency toCurrency) {
        String mapKey = this.constructMapKey(fromCurrency, toCurrency);
        if (!this.exchangeRateMap.containsKey(mapKey)) {
            throw new IllegalArgumentException("Currency not supported");
        }
        return exchangeRateMap.get(mapKey);
    }

    private String constructMapKey(Currency from, Currency to) {
        return from + "-" + to;
    }
}
