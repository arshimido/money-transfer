package com.revolut.moneytransfer.services;

import com.revolut.moneytransfer.daos.AccountDao;
import com.revolut.moneytransfer.dto.TransferRequestDTO;
import com.revolut.moneytransfer.exceptions.*;
import com.revolut.moneytransfer.models.Account;
import com.revolut.moneytransfer.models.Currency;
import com.revolut.moneytransfer.models.Transaction;
import com.revolut.moneytransfer.utils.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class AccountService {


    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    private final CurrencyExchangeService currencyExchangeService;
    private final TransactionService transactionService;
    private final AccountDao accountDao;

    public AccountService(AccountDao accountDao, CurrencyExchangeService currencyExchangeService,
                           TransactionService transactionService) {
        this.accountDao = accountDao;
        this.currencyExchangeService = currencyExchangeService;
        this.transactionService = transactionService;
    }

    /**
     * Retrieves {@code Account} by id
     *
     * @param id {@code Long} id of account to retrieve
     *
     * @return {@code Optional<Account>} contains retrieved {@code Account}. Or empty optional if none exists.
     */
    public Optional<Account> getAccountById(Long id) {
        return this.accountDao.findById(id);
    }


    /**
     * List all accounts
     *
     * @return {@code List<Account>} list of all accounts.
     */
    public List<Account> list() {
        return this.accountDao.list();
    }

    /**
     * Validates the specified {@code TransferRequestDTO} and executes the transfer.
     *
     * @param transferRequestDTO {@code TransferRequestDTO} DTO containing transfer order parameters.
     *
     * @throws CurrencyNotSupportedException If specified currency is not supported in our system.
     * @throws TransferLimitExceeded If Account has exceeded the Transfer limits.
     * @throws AccountDoesNotExistException If either of the accounts doesn't exists. Or transfer to the same account.
     * @throws InSufficientBalanceException If sender account doesn't have sufficient balance to execute transfer.
     * @throws EntityNotFoundException If required DB entity doesn't exist.
     */
    public synchronized void transferMoney(TransferRequestDTO transferRequestDTO)
            throws CurrencyNotSupportedException, TransferLimitExceeded, AccountDoesNotExistException,
            InSufficientBalanceException, EntityNotFoundException {

        logger.info("Validating transfer request");
        validateRequest(transferRequestDTO);

        logger.info("Transfer request is valid");
        Account fromAccount = this.accountDao.findById(transferRequestDTO.getFromAccountId()).get();
        Account toAccount = this.accountDao.findById(transferRequestDTO.getToAccountId()).get();

        Transaction transaction =
                this.transactionService.createNewTransferTransaction(fromAccount, toAccount,
                transferRequestDTO.getAmount(), Currency.valueOf(transferRequestDTO.getCurrency()));
        logger.info("Pending transaction has been created with id: {}", transaction.getId());


        try {
            logger.info("Executing transfer");
            this.accountDao.executeTransfer(transferRequestDTO.getFromAccountId(), transferRequestDTO.getToAccountId(), transaction.getId());
        } catch (Exception e) {
            logger.error("Failed to executed the transfer", e);
            this.transactionService.markTransactionAdFailed(transaction.getId());
            throw e;
        }
    }

    /**
     * Validated the specified {@code TransferRequestDTO}
     *
     * @param transferRequest {@code TransferRequestDTO} DTO to be validated
     *
     * @throws CurrencyNotSupportedException If specified currency is not supported in our system.
     * @throws TransferLimitExceeded If Account has exceeded the Transfer limits.
     * @throws AccountDoesNotExistException If either of the accounts doesn't exists. Or transfer to the same account.
     * @throws InSufficientBalanceException If sender account doesn't have sufficient balance to execute transfer.
     */
    private void validateRequest(TransferRequestDTO transferRequest)
            throws AccountDoesNotExistException, CurrencyNotSupportedException,
            TransferLimitExceeded, InSufficientBalanceException {

        if (transferRequest.getFromAccountId() == null) {
            throw new IllegalArgumentException("Sender account ID can not be null");
        }

        if (transferRequest.getToAccountId() == null) {
            throw new IllegalArgumentException("Receiver account ID can not be null");
        }

        Optional<Account> fromAccount = this.getAccountById(transferRequest.getFromAccountId());
        if (!fromAccount.isPresent()) {
            throw new AccountDoesNotExistException("Invalid sender account ID");
        }

        Optional<Account> toAccount = this.getAccountById(transferRequest.getToAccountId());
        if (!toAccount.isPresent() || transferRequest.getFromAccountId().equals(transferRequest.getToAccountId())) {
            throw new AccountDoesNotExistException("Invalid receiver account ID");
        }

        Currency currency = this.parseAndValidateCurrency(transferRequest.getCurrency());

        this.verifyBalance(fromAccount.get(), transferRequest.getAmount(), currency);
        this.verifyTransferLimits(fromAccount.get(), transferRequest.getAmount(), currency);
    }


    /**
     * Parses currency input and verifies whether its supported currency.
     *
     * @param currency {@code String} currency string input to be verified
     * @return {@code Currency} Corresponding currency
     * @throws CurrencyNotSupportedException if specified string represents a currency that is not supported.
     */
    private Currency parseAndValidateCurrency(String currency) throws CurrencyNotSupportedException {
        try {
            return Currency.valueOf(currency);
        } catch (IllegalArgumentException e) {
            logger.error("Currency not found", e);
            throw new CurrencyNotSupportedException("Invalid currency");
        }
    }

    /**
     * Verify that sender's account hasn't exceeded the transfer limits for toady.
     * Starting from 00:00:00 same day till this request execution time.
     *
     * Transfer limits are calculated in the Account's currency.
     *
     * Note: Its not a moving 24-hours window.
     *
     * @param fromAccount {@code Account} Sender's account
     * @param amount {@code BigDecimal} amount of this transfer request
     * @param currency {@code Currency} currency of the transfer amount
     *
     * @throws TransferLimitExceeded If account exceeded (or will exceed including the current transfer amount) the transfer limits.
     */
    private void verifyTransferLimits(Account fromAccount, BigDecimal amount, Currency currency)
            throws TransferLimitExceeded, CurrencyNotSupportedException {

        logger.info("Verifying transfer limits for account id: {}", fromAccount.getId());
        BigDecimal transferAmount = amount;
        if (!fromAccount.getCurrency().equals(currency)) {
            BigDecimal exchangeRate = this.currencyExchangeService.getExchangeRate(currency, fromAccount.getCurrency());
            transferAmount = transferAmount.multiply(exchangeRate);

            logger.info("Currencies are different. exchange rate is: {}", exchangeRate);
        }

        if (transferAmount.compareTo(new BigDecimal(ApplicationProperties.DAILY_TRANSFER_LIMIT)) > 0) {
            throw new TransferLimitExceeded("Transfer limit exceeded");
        }


        List<Transaction> trxList = this.transactionService.findTransferTransactionsForAccountAsSender(fromAccount, LocalDate.now().atStartOfDay());
        logger.info("Found {} successful transfer transactions", trxList.size());

        // add all transactions since beginning of the day to the transfer amount in order to check transfer limit
        transferAmount = transferAmount.add(
                trxList.stream().map(Transaction::getAmountInSenderCurrency).reduce(BigDecimal.ZERO, BigDecimal::add));
        if (transferAmount.compareTo(new BigDecimal(ApplicationProperties.DAILY_TRANSFER_LIMIT)) > 0) {
            throw new TransferLimitExceeded("Transfer limit exceeded");
        }
    }

    /**
     * Verify sender's account balance has sufficient money to execute the transfer request.
     *
     * @param account {@code Account} sender;s account
     * @param amount {@code BigDecimal} desired transfer amount
     * @param currency {@code Currency} transfer currency
     *
     * @throws InSufficientBalanceException If sender's account doesn't have enough money to execute the transfer.
     */
    private void verifyBalance(Account account, BigDecimal amount, Currency currency) throws InSufficientBalanceException {
        BigDecimal amountInSenderCurrency = amount;
        if (!currency.equals(account.getCurrency())) {
            amountInSenderCurrency = amount.multiply(
                    this.currencyExchangeService.getExchangeRate(currency, account.getCurrency()));
        }

        if (account.getBalance().compareTo(amountInSenderCurrency) < 0) {
            throw new InSufficientBalanceException("Insufficient balance");
        }
    }
}
