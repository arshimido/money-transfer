package com.revolut.moneytransfer.services;

import com.revolut.moneytransfer.daos.TransactionDao;
import com.revolut.moneytransfer.exceptions.EntityNotFoundException;
import com.revolut.moneytransfer.models.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);

    private final TransactionDao transactionDao;
    private final CurrencyExchangeService currencyExchangeService;

    public TransactionService(TransactionDao transactionDao, CurrencyExchangeService currencyExchangeService) {
        this.transactionDao = transactionDao;
        this.currencyExchangeService = currencyExchangeService;
    }

    public List<Transaction> findTransferTransactionsForAccountAsSender(Account account, LocalDateTime since) {
        return this.transactionDao.findTransferTransactionsFromAccountSince(account, since);
    }

    public Transaction createNewTransferTransaction(Account from, Account to, BigDecimal amount, Currency currency) {
        Transaction transaction = new Transaction();
        transaction.setType(TransactionType.TRANSFER);
        transaction.setStatus(TransactionStatus.PENDING);
        transaction.setSender(from);
        transaction.setReceiver(to);
        transaction.setAmount(amount);
        transaction.setCurrency(currency);
        transaction.setSenderCurrency(from.getCurrency());
        transaction.setReceiverCurrency(to.getCurrency());

        if (!from.getCurrency().equals(currency)) {
            BigDecimal exchangeRate = this.currencyExchangeService.getExchangeRate(currency, from.getCurrency());
            transaction.setAmountInSenderCurrency(exchangeRate.multiply(amount));
            transaction.setExchangeRateToSenderCurrency(exchangeRate);
        } else {
            transaction.setAmountInSenderCurrency(amount);
            transaction.setExchangeRateToSenderCurrency(BigDecimal.ONE);
        }

        if (!to.getCurrency().equals(currency)) {
            BigDecimal exchangeRate = this.currencyExchangeService.getExchangeRate(currency, to.getCurrency());
            transaction.setAmountInReceiverCurrency(exchangeRate.multiply(amount));
            transaction.setExchangeRateToReceiverCurrency(exchangeRate);
        } else {
            transaction.setAmountInReceiverCurrency(amount);
            transaction.setExchangeRateToReceiverCurrency(BigDecimal.ONE);
        }

        Long id = this.transactionDao.saveTransaction(transaction);
        return this.transactionDao.findById(id).get();
    }

    public void markTransactionAdFailed(Long id) throws EntityNotFoundException {
        Optional<Transaction> transaction = this.transactionDao.findById(id);
        if (!transaction.isPresent()) {
            throw new EntityNotFoundException("No transaction found with id: " + id);
        }

        Transaction trx = transaction.get();
        trx.setStatus(TransactionStatus.FAILED);
        this.transactionDao.updateTransaction(trx);
    }
}
