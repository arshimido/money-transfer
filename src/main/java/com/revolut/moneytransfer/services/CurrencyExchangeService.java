package com.revolut.moneytransfer.services;

import com.revolut.moneytransfer.exceptions.CurrencyNotSupportedException;
import com.revolut.moneytransfer.models.Currency;

import java.math.BigDecimal;

public interface CurrencyExchangeService {
    /**
     * Retrieves exchange rate from Currency to another
     *
     * @param fromCurrency {@code Currency} currency to retrieve exchange rate from
     * @param toCurrency {@code Currency}
     *
     * @return {@code BigDecimal} exchange rate from {@code fromCurrency} to {@code toCurrency}
     */
    BigDecimal getExchangeRate(Currency fromCurrency, Currency toCurrency);
}
