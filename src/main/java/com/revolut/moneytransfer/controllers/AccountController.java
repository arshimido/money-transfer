package com.revolut.moneytransfer.controllers;

import com.revolut.moneytransfer.dto.AccountDTO;
import com.revolut.moneytransfer.dto.ErrorDTO;
import com.revolut.moneytransfer.dto.TransferRequestDTO;
import com.revolut.moneytransfer.exceptions.*;
import com.revolut.moneytransfer.models.Account;
import com.revolut.moneytransfer.services.AccountService;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.stream.Collectors;


public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private final AccountService accountService;

    public AccountController(final AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * lists all accounts
     *
     * @param context {@code Context} Request context object
     */
    public void list(Context context) {
        logger.info("Listing accounts ...");
        context.json(accountService.list().stream().map(AccountDTO::fromAccount).collect(Collectors.toList()));
    }

    /**
     * Retrieves Account by ID
     *
     * @param context {@code Context} Request context object
     */
    public void getAccount(Context context) {
        logger.info("Retrieving account");
        Long id;
        try {
            id = Long.valueOf(context.pathParam("accountId"));
        } catch (NumberFormatException e) {
            logger.error("Invalid parameter 'accountId'", e);
            context.status(400).json(new ErrorDTO("Invalid parameter 'accountId'"));
            return;
        }


        Optional<Account> account = accountService.getAccountById(id);
        if (account.isPresent()) {
            context.json(AccountDTO.fromAccount(account.get()));
            return;
        }

        context.json(new ErrorDTO("Entity not found for id: " + id)).status(404);
    }

    /**
     *  {@code Context} Request context object
     *
     * @param context
     *
     * @throws CurrencyNotSupportedException
     * @throws AccountDoesNotExistException
     * @throws TransferLimitExceeded
     * @throws EntityNotFoundException
     * @throws InSufficientBalanceException
     */
    public void transferMoney(Context context)
            throws CurrencyNotSupportedException, AccountDoesNotExistException, TransferLimitExceeded,
            EntityNotFoundException, InSufficientBalanceException {

        TransferRequestDTO transferRequestDTO = context.bodyAsClass(TransferRequestDTO.class);
        accountService.transferMoney(transferRequestDTO);
        context.status(200);
    }
}
