# Technology choices
 - Java 8 and Maven 
 - Javalin (https://javalin.io/) lightweight Web framework.
 - Hibernate (http://hibernate.org/) for database communication
 - H2 (https://h2database.com/html/main.html) In-memory DB
 - Junit, Mockito, Rest-assured for unit and API testing
 
## Prerequisites
 - Java & maven installed
 
 
## How to run tests
 ```bash
 mvn clean install
 mvn clean test
```
 
## How to run Application
**applicatiom starts on port 7000** 
 ```bash
 mvn clean install
 mvn exec:java -Dexec.mainClass="com.revolut.moneytransfer.Application"
```

## API documentation
- You can find API documentation in OpenAPI format by starting the server and visting this URL: **http://localhost:7000/openapi**
- For more UI-friendly format (https://redoc.ly) you can find it here: **http://localhost:7000/redoc**

## Thoughts and Miscellaneous Notes:
- Once application starts it seeds database with data from file **resources/data.sql**
- I use DTOs for data transfer between controller and services and vice-versa in order to hide DB models (it may contain sensitive information).
- **ApplicationProperties** class contains the properties that ideally should be read from environment variables.
- **Currency** Ideally should be a DB entity as well. I kept it as Enum for simplicity as well.
- Included under **coverage** folder, HTML coverage report for the application.


## API Examples:

#### List accounts:
```bash
curl http://localhost:7000/api/v1/accounts
```

#### Get account details:
Request:
```bash
curl http://localhost:7000/api/v1/accounts/1
```

Response:
```json
{
    "id": 1,
    "ownerId": 10,
    "accountNumber": "abc-001-123",
    "balance": 1991.0400,
    "currency": "EUR",
    "creationDate": "2020-01-16 03:29:05"
}
```


#### TransferMoney:
Request:
```bash
curl --request POST --data '{"fromAccountId": 1, "toAccountId": 2, "amount": 10, "currency": "USD"}' http://localhost:7000/api/v1/accounts/transfer
```

Success Response:
- response code **200**
- No body

Sample invalid request and error response:
```bash
curl --request POST --data '{"fromAccountId": 1, "toAccountId": 2, "amount": 1000000, "currency": "USD"}' http://localhost:7000/api/v1/accounts/transfer
```

Response Code: 422 (Unprocessable entity)
```json
{
    "message": "Insufficient balance"
}
```


